package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.services.UpdateCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;

@Component
public class BackController implements Controller{

    UpdateCache updateCache;


    @Autowired
    public BackController(UpdateCache updateCache) {
        this.updateCache = updateCache;
    }

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update) throws TelegramApiException {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();
        Integer msgId = update.getCallbackQuery().getMessage().getMessageId();
        long chatid = update.getCallbackQuery().getMessage().getChatId();

        if (msgId.equals(updateCache.getPreviousInlineMessageId())) {
            EditMessageReplyMarkup markup = updateCache.getPreviousInlineKeyboard(msgId);
            markup.setChatId(chatid);
            responseToSend.add(markup);
        }
        else  {
            SendMessage msg = new SendMessage();

            msg.setText("Сообщение устарело");
            msg.setChatId(chatid);
            responseToSend.add(msg);
        }

        return responseToSend;
    }

}
