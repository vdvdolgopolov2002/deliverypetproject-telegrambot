package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.keyboards.InlineKeyboards;
import com.vdurmont.emoji.EmojiParser;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


public interface Controller {

    List<BotApiMethod<?>> processUpdate (Update update) throws TelegramApiException;

    default List<BotApiMethod<?>> processUpdate (Update update, String command) throws TelegramApiException {
        throw new TelegramApiException("Не поддерживается");
    }



    default SendMessage buildSendMessage(long chatid, String text, ReplyKeyboard keyboard) {
        SendMessage response = new SendMessage();

        response.setText(EmojiParser.parseToUnicode(text));
        response.setChatId(chatid);
        response.setReplyMarkup(keyboard);

        return response;
    }
}
