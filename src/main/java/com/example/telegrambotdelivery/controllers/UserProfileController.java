package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.keyboards.InlineKeyboards;
import com.example.telegrambotdelivery.services.CustomerService;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserProfileController implements Controller{

    private static CustomerService customerService;

    @Autowired
    public UserProfileController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update) throws TelegramApiException {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();
        if(update.hasCallbackQuery()){
            BotApiMethod<?> response = handleProfileCallBack(update);
            responseToSend.add(response);
        }
        else {
            SendMessage response = userProfileMessage(update);
            responseToSend.add(response);
        }

        return responseToSend;
    }

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update, String command) throws TelegramApiException {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();

        long chatid = update.getMessage().getChatId();

        if (command.startsWith(Commands.PROFILE_UPDATE.getCommandName())){

            SendMessage response = new SendMessage();
            command = command.replace(Commands.PROFILE_UPDATE.getCommandName(), "");
            String updateText = update.getMessage().getText();

            switch (command){
                case "/name":
                    customerService.updateCustomerName(chatid, updateText);
                    break;
                case "/phone":
                    customerService.updateCustomerPhone(chatid, updateText);
                    break;
                case "/location":
                    customerService.updateCustomerLocation(chatid, updateText);
                    break;
                case "/payment":
                    customerService.updateCustomerPayment(chatid, updateText);
                    break;

            }
            response = userProfileMessage(update);
            responseToSend.add(response);
        }

        return  responseToSend;
    }

    public SendMessage userProfileMessage (Update update) throws TelegramApiException {
        long chatid = update.getMessage().getChatId();
        SendMessage response = new SendMessage();

        if(customerService.getCustomerByChatID(chatid) != null) {
            String profileText = customerService.getCustomerProfileText(chatid);
            response = buildSendMessage(chatid, profileText, InlineKeyboards.getProfileInlineMarkup());
        }
        return response;
    }

    public BotApiMethod<?> handleProfileCallBack (Update update) throws TelegramApiException {

        long chatid = update.getCallbackQuery().getMessage().getChatId();
        String callbackCommand = update.getCallbackQuery().getData();

        if (callbackCommand.startsWith(Commands.PROFILE_UPDATE.getCommandName())){
            SendMessage response = new SendMessage();

            callbackCommand = callbackCommand.replace(Commands.PROFILE_UPDATE.getCommandName(), "");

            switch (callbackCommand){
                case "/name":
                    response = buildSendMessage(chatid, "Введите новое имя, не более 50 символов и 4 слов", new ReplyKeyboardRemove(true));
                break;
                case "/phone":
                    response = buildSendMessage(chatid, "Введите новый телефон в формате +7/89ххххххххх", new ReplyKeyboardRemove(true));
                break;
                case "/location":
                    response = buildSendMessage(chatid, "Введите новый адрес в формате Город, Улица, Номер дома(буква, корпус), Квартира(если есть) через запятую", new ReplyKeyboardRemove(true));
                break;
                case "/payment":
                    response = buildSendMessage(chatid, "Введите данные новой карты в формате ХХХХХХХХХХХХХХХХ (16 символов), ИМЯ ВЛАДЕЛЬЦА", new ReplyKeyboardRemove(true));
                break;

            }

            return response;
        }
        else if (callbackCommand.startsWith(Commands.PROFILE.getCommandName())) {

            CallbackQuery callbackQuery = update.getCallbackQuery();
            EditMessageReplyMarkup editMessageReplyMarkup = new EditMessageReplyMarkup();

            editMessageReplyMarkup.setChatId(callbackQuery.getMessage().getChatId());
            editMessageReplyMarkup.setMessageId(callbackQuery.getMessage().getMessageId());
            editMessageReplyMarkup.setReplyMarkup(InlineKeyboards.profileUpdateKeyborMaker(callbackCommand));

            return editMessageReplyMarkup;
        }
        else {
            SendMessage response = buildSendMessage(chatid, "Ошибка", new ReplyKeyboardRemove(true));
            return response;
        }
    }

}
