package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.config.DeliveryBotConfig;
import com.example.telegrambotdelivery.services.UpdateCache;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class DeliveryMainBot extends TelegramLongPollingBot  {

    private ControllerContainer controllerContainer;
    final DeliveryBotConfig config;
    private final TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
    public UpdateCache updateCache;


    @Autowired
    public DeliveryMainBot(ControllerContainer controllerContainer, DeliveryBotConfig config, UpdateCache updateCache) throws TelegramApiException {
        super(config.getToken());
        this.config = config;
        this.controllerContainer = controllerContainer;
        this.updateCache = updateCache;
    }

    @PostConstruct
    public void init() throws TelegramApiException {
        telegramBotsApi.registerBot(this);
    }

    @Override
    public  String getBotToken(){
        return config.getToken();
    }

    @Override
    public String getBotUsername() {
        return config.getBotName();
    }

    @Override
    public void onUpdateReceived(Update update) {

        updateCache.addUpdateToCache(update);

        List <BotApiMethod<?>> responsesToSend = updateHandler(update);

        for (BotApiMethod<?> response : responsesToSend) {
            try {
                  execute(response);
            } catch (TelegramApiException e) {
                e.getMessage();
            }
        }
    }

    public List <BotApiMethod<?>> updateHandler (Update update) {
        List<BotApiMethod<?>> responsesToSend = new ArrayList<>();


        try {
            if (update.hasCallbackQuery()) {
                String messageText = update.getCallbackQuery().getData();
                Controller controller;
                if (messageText.startsWith(Commands.ORDER.getCommandName())) {
                    controller = controllerContainer.getController(Commands.ORDER.getCommandName());
                }
                else if (messageText.startsWith(Commands.CART.getCommandName())){
                    controller = controllerContainer.getController(Commands.CART.getCommandName());
                }
                else {
                    controller = controllerContainer.getController(messageText);
                }
                responsesToSend = controller.processUpdate(update);
            }
            else if (updateHasCommand(update)) {
                String messageText = getCommandFromUpdate(update);
                Controller controller = controllerContainer.getController(messageText);
                responsesToSend = controller.processUpdate(update);
            }
            else {
                String messageText = updateCache.getLastCallBackCommand();
                Controller controller = controllerContainer.getController(messageText);
                responsesToSend = controller.processUpdate(update, messageText);
            }

        } catch (TelegramApiException e) {
            SendMessage sendMessage = new SendMessage();
            long chatId;
            if (update.hasCallbackQuery()){chatId = update.getCallbackQuery().getMessage().getChatId();}
            else {chatId = update.getMessage().getChatId();}
            sendMessage.setChatId(chatId);
            sendMessage.setText(e.getMessage());
            responsesToSend.add(sendMessage);
        }

        return responsesToSend;
    }

    public String getCommandFromUpdate(Update update) {
        String msg = update.getMessage().getText();

        String command = "";

        // Regex pattern to match symbols after '/' until whitespace.
        String regex = "/\\S+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(msg);

        if (matcher.find()) {
            command = matcher.group();
        }

        return command;
    }

    public Boolean updateHasCommand(Update update) {
        if (getCommandFromUpdate(update).equals("")) {
            return false;
        }
        return true;
    }






}
