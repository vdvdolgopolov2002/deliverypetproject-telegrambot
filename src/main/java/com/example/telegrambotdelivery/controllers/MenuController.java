package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.entities.Product;
import com.example.telegrambotdelivery.keyboards.InlineKeyboards;
import com.example.telegrambotdelivery.services.CustomerService;
import com.example.telegrambotdelivery.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class MenuController implements Controller{

    private CustomerService customerService;
    private ProductService productService;

    @Autowired
    public MenuController(ProductService productService, CustomerService customerService) {
        this.productService = productService;
        this.customerService = customerService;
    }

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update) throws TelegramApiException {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();

        if(update.hasCallbackQuery()){
            BotApiMethod<?> response = handleMenuCallBack(update);
            responseToSend.add(response);
        }
        else {
            SendMessage response = menuMessage(update);
            responseToSend.add(response);
        }

        return responseToSend;
    }

    private BotApiMethod<?> handleMenuCallBack(Update update) {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();
        long chatid = update.getCallbackQuery().getMessage().getChatId();
        String callbackCommand = update.getCallbackQuery().getData();

        if (callbackCommand.startsWith(Commands.ORDER.getCommandName())){
            callbackCommand = callbackCommand.replace("/order/", "");
            Long productid = Long.parseLong(callbackCommand);
            customerService.addToCart(chatid, productid);
            SendMessage response = buildSendMessage(chatid, "Добавлено в корзину", new ReplyKeyboardRemove(true));
            responseToSend.add(response);
            return response;
        }
        else if (callbackCommand.startsWith(Commands.MENU.getCommandName())) {
            callbackCommand = callbackCommand.replace("/menu/", "");

            CallbackQuery callbackQuery = update.getCallbackQuery();
            EditMessageReplyMarkup editMessageReplyMarkup = new EditMessageReplyMarkup();

            editMessageReplyMarkup.setChatId(callbackQuery.getMessage().getChatId());
            editMessageReplyMarkup.setMessageId(callbackQuery.getMessage().getMessageId());
            editMessageReplyMarkup.setReplyMarkup(InlineKeyboards.menuCategoryKeyboard(productService.getProductsToCategoryMap().get(callbackCommand)));
            return editMessageReplyMarkup;
        }
        else {
            SendMessage response = buildSendMessage(chatid, "Ошибка", new ReplyKeyboardRemove(true));
            return response;
        }
    }


    @Override
    public List<BotApiMethod<?>> processUpdate(Update update, String command) throws TelegramApiException {
        return Controller.super.processUpdate(update, command);
    }

    private SendMessage menuMessage(Update update) {
        long chatid = update.getMessage().getChatId();
        SendMessage response;
        String menuText = productService.getMenuText();
        response = buildSendMessage(chatid, menuText, InlineKeyboards.menuKeyboard());
        return response;
    }
}
