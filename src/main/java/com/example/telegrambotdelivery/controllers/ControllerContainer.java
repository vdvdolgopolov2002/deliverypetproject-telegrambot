package com.example.telegrambotdelivery.controllers;


import com.example.telegrambotdelivery.entities.ProductCategory;
import com.example.telegrambotdelivery.services.CustomerService;
import com.example.telegrambotdelivery.services.ProductService;
import com.example.telegrambotdelivery.services.UpdateCache;
import jakarta.annotation.PostConstruct;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class ControllerContainer {


    private HashMap<String, Controller> commandMap;
    private Controller unknownCommandController = new UnknownCommandController();
    UpdateCache updateCache;
    CustomerService customerService;
    ProductService productService;


    public ControllerContainer(UpdateCache updateCache, CustomerService customerService, ProductService productService) {
        this.customerService = customerService;
        this.updateCache = updateCache;
        this.productService = productService;
    }

    public Controller getController(String command) {
        return commandMap.getOrDefault(command, unknownCommandController);
    }

    @PostConstruct
    public void init() {
        commandMap = new HashMap<>();
        commandMap.put(Commands.START.getCommandName(), new StartController(customerService));
        commandMap.put(Commands.REGISTER.getCommandName(), new RegisterController(customerService));
        commandMap.put(Commands.PROFILE.getCommandName(), new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE.getCommandName() + "/name", new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE.getCommandName() + "/phone", new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE.getCommandName() + "/location", new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE.getCommandName() + "/payment", new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE_UPDATE.getCommandName() + "/name", new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE_UPDATE.getCommandName() + "/phone", new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE_UPDATE.getCommandName() + "/location", new UserProfileController(customerService));
        commandMap.put(Commands.PROFILE_UPDATE.getCommandName() + "/payment", new UserProfileController(customerService));
        commandMap.put(Commands.MENU.getCommandName(), new MenuController(productService, customerService));
        commandMap.put(Commands.MENU.getCommandName() + "/" + ProductCategory.BURGERS.getCategoryName(), new MenuController(productService, customerService));
        commandMap.put(Commands.MENU.getCommandName() + "/" + ProductCategory.APPETISERS.getCategoryName(), new MenuController(productService, customerService));
        commandMap.put(Commands.MENU.getCommandName() + "/" + ProductCategory.SAUCE.getCategoryName(), new MenuController(productService, customerService));
        commandMap.put(Commands.ORDER.getCommandName(), new MenuController(productService, customerService));
        commandMap.put(Commands.BACK.getCommandName(), new BackController(updateCache));
        commandMap.put(Commands.CART.getCommandName(), new CartController(customerService, productService));
    }


    public HashMap<String, Controller> getCommandMap () {
        return commandMap;
    }

}
