package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.keyboards.InlineKeyboards;
import com.example.telegrambotdelivery.services.CustomerService;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class RegisterController implements Controller {

    private static CustomerService customerService;

    @Autowired
    public RegisterController(CustomerService customerService) {
        this.customerService = customerService;
    }

    public static String REG_TEXT = """
            :hamburger: :hamburger: :hamburger:
            Давайте заполним ваш профиль.
            Для заказа доставки необходимо заполнить:
            
            :telephone_receiver: номер телефона
            
            :bust_in_silhouette: имя
            
            :house_with_garden: геолокацию
            
            :credit_card: данные платежной системы
            
            """;

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update) throws TelegramApiException {

        List <BotApiMethod<?>> responseToSend = new ArrayList<>();

        long chatid = update.getMessage().getChatId();


        customerService.registerNewCustomer(update, chatid);

        SendMessage response1 = buildSendMessage(chatid, REG_TEXT, new ReplyKeyboardRemove(true));
        responseToSend.add(response1);

        SendMessage response2 = buildSendMessage(chatid, customerService.getCustomerProfileText(chatid), InlineKeyboards.getProfileInlineMarkup());
        responseToSend.add(response2);

        return responseToSend;

    }
}
