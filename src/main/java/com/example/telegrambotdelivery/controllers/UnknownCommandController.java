package com.example.telegrambotdelivery.controllers;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class UnknownCommandController implements Controller{

    public UnknownCommandController() {
    }

    public static String UNSUPPORTED_TEXT = """
            Действие не поддерживается
            """;

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update) throws TelegramApiException {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();
        long chatid = update.getMessage().getChatId();

        SendMessage response = new SendMessage();
        response.setText(UNSUPPORTED_TEXT);
        response.setChatId(chatid);
        responseToSend.add(response);

        return responseToSend;
    }
}
