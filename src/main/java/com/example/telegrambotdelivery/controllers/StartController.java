package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.config.DeliveryBotConfig;
import com.example.telegrambotdelivery.services.CustomerService;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class StartController implements Controller{

    private static CustomerService customerService;

    @Autowired
    public StartController(CustomerService customerService) {
        this.customerService = customerService;
    }

    public static String START_TEXT = """
            :wave: Добро пожаловать!
            Нажми 
            /menu , чтобы выбрать блюда.
            /profile , чтобы посмотреть профиль.
            /cart , чтобы посмотреть корзину.
            /help , для подсказок.
            """;

    public static String START_UNAUTH_TEXT = """
            :wave: Добро пожаловать!
            Что-то я тебя не помню...
            Нажми 
            /register , чтобы зарегистрироваться.
            """;

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update) throws TelegramApiException {

        List <BotApiMethod<?>> responseToSend = new ArrayList<>();

        long chatid = update.getMessage().getChatId();

        if (customerService.getCustomerByChatID(chatid) != null) {
            SendMessage response = buildSendMessage(chatid, START_TEXT, new ReplyKeyboardRemove(true));
            responseToSend.add(response);
        }

        else {
            SendMessage response = buildSendMessage(chatid, START_UNAUTH_TEXT, new ReplyKeyboardRemove(true));
            responseToSend.add(response);
        }

        return responseToSend;
    }
}
