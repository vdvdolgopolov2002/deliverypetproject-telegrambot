package com.example.telegrambotdelivery.controllers;

import com.example.telegrambotdelivery.entities.CartItem;
import com.example.telegrambotdelivery.services.CustomerService;
import com.example.telegrambotdelivery.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CartController implements Controller{

    private CustomerService customerService;
    private ProductService productService;

    @Autowired
    public CartController(CustomerService customerService, ProductService productService) {
        this.customerService = customerService;
        this.productService = productService;
    }

    @Override
    public List<BotApiMethod<?>> processUpdate(Update update) throws TelegramApiException {

        List <BotApiMethod<?>> responseToSend = new ArrayList<>();

        if(update.hasCallbackQuery()){
            responseToSend = handleCartCallBack(update);
        }
        else {
            responseToSend = cartMessages(update);
        }

        return responseToSend;
    }

    private  List <BotApiMethod<?>> handleCartCallBack(Update update) throws TelegramApiException {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();
        long chatid = update.getCallbackQuery().getMessage().getChatId();
        String callbackCommand = update.getCallbackQuery().getData();

        if (callbackCommand.startsWith(Commands.CART.getCommandName())){
            callbackCommand = callbackCommand.replace("/cart", "");

            if (callbackCommand.startsWith("/add/")) {
                callbackCommand = callbackCommand.replace("/add/", "");
                Long productid = Long.parseLong(callbackCommand);
                customerService.addToCart(chatid, productid);
                SendMessage response = buildSendMessage(chatid, "Добавлено в корзину", new ReplyKeyboardRemove(true));
                responseToSend.add(response);
            }
            else if(callbackCommand.startsWith("/remove/")) {
                callbackCommand = callbackCommand.replace("/remove/", "");
                Long productid = Long.parseLong(callbackCommand);
                customerService.removeFromCart(chatid, productid);
                SendMessage response = buildSendMessage(chatid, "Удалено из корзины", new ReplyKeyboardRemove(true));
                responseToSend.add(response);
            }
            else if(callbackCommand.startsWith("/clear")) {
                customerService.removeAllFromCart(chatid);
                SendMessage response = buildSendMessage(chatid, "Корзина очищена", new ReplyKeyboardRemove(true));
                responseToSend.add(response);
            }
            else if(callbackCommand.startsWith("/createorder")) {
                customerService.createNewOrder(chatid);
                SendMessage response = buildSendMessage(chatid, "Спасибо!\nМы передали заказ в ресторан, вы получите уведомления об изменениях статуса заказа" , new ReplyKeyboardRemove(true));
                responseToSend.add(response);
            }
        }
        return responseToSend;
    }

    private List<BotApiMethod<?>> cartMessages(Update update) {
        List <BotApiMethod<?>> responseToSend = customerService.getCustomersCartMessages(update.getMessage().getChatId());
        return responseToSend;
    }


}
