package com.example.telegrambotdelivery.controllers;


public enum Commands {
    START("/start"),
    REGISTER("/register"),
    MENU("/menu"),
    PROFILE("/profile"),
    HELP("/help"),
    PROFILE_UPDATE("/profile/update"),
    BACK("/back"),
    ORDER("/order"),
    CART("/cart");

    private final String commandName;

    Commands(String commandName) {
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }

}
