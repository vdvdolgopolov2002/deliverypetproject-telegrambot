package com.example.telegrambotdelivery.entities;

public enum ProductCategory {
    BURGERS("Бургеры"),
    APPETISERS("Закуски"),
    SAUCE("Соусы");


    private final String categoryName;

    ProductCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

}
