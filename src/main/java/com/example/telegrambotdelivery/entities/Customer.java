package com.example.telegrambotdelivery.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


@Data
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "phonenumber")
    private String number;

    @Column(name = "location")
    private String customerLocation;

    @Column(name = "payment")
    private String payment;

    @Column(name = "chatid")
    private long chatid;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id")
    private Cart cart;


}
