package com.example.telegrambotdelivery.entities;

public enum OrderStatus {
    PROCESSING("processing"),
    COOKING("cooking"),
    DELIVERING("delivering"),
    DELIVERED("delivered"),
    CLOSED("closed");

    private final String status;

    OrderStatus(String status) {this.status = status;}

    public String getStatus() {
        return status;
    }
}
