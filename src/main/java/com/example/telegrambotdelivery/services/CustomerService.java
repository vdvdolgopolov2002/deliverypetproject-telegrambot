package com.example.telegrambotdelivery.services;

import com.example.telegrambotdelivery.entities.CartItem;
import com.example.telegrambotdelivery.entities.Customer;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

@Service
public interface CustomerService {

    public void registerNewCustomer(Update update, long chatid) throws TelegramApiException;

    public Customer getCustomerByChatID(long chatid);

    public String getCustomerProfileText(long chatid) throws TelegramApiException;

    public void updateCustomerPhone(long chatid, String number) throws TelegramApiException;

    public void updateCustomerName(long chatid, String name) throws TelegramApiException;

    public void updateCustomerLocation(long chatid, String location) throws TelegramApiException;

    public void updateCustomerPayment(long chatid, String payment) throws TelegramApiException;

    public void addToCart(long chatid, Long productid);

    public void removeFromCart(long chatid, Long productid) throws TelegramApiException;
    
    public List<CartItem> getCustomersCartItems(long chatid);

    public List<BotApiMethod<?>> getCustomersCartMessages(long chatid);
    
    public void removeAllFromCart(long chatid);

    public void createNewOrder(long chatid) throws TelegramApiException;
}
