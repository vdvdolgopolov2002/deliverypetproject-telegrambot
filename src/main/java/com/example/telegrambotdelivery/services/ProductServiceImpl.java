package com.example.telegrambotdelivery.services;

import com.example.telegrambotdelivery.entities.Product;
import com.example.telegrambotdelivery.entities.ProductCategory;
import com.example.telegrambotdelivery.repositories.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService{

    ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Map<String, List<Product>> getProductsToCategoryMap() {
        Map<String, List<Product>> productByCategory = productRepository.getAllBy().stream().collect(Collectors.groupingBy(product -> product.getProductCategory().getCategoryName()));
        return productByCategory;
    }

    @Override
    public String getMenuText() {
        Map<String, List<Product>> productByCategory = getProductsToCategoryMap();
        StringBuilder stringBuilder = new StringBuilder();
        for(String category : productByCategory.keySet()){
            stringBuilder.append(category.toUpperCase());
            stringBuilder.append("\n");
            stringBuilder.append("\n");
            for (Product product : productByCategory.get(category)) {
                stringBuilder.append("\t");
                stringBuilder.append(product.getName() + " ... :money_with_wings: " + product.getPrice().toString() + " рублей");
                stringBuilder.append("\n");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public Product getProductById (Long id) {
        if (productRepository.getProductById(id) != null) {
            return productRepository.getProductById(id);
        }
        else throw new EntityNotFoundException("product not found");
    }

}
