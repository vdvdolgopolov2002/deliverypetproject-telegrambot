package com.example.telegrambotdelivery.services;

import com.example.telegrambotdelivery.entities.Product;
import com.example.telegrambotdelivery.entities.ProductCategory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public interface ProductService {

    public Map<String, List<Product>> getProductsToCategoryMap();


    String getMenuText();
}
