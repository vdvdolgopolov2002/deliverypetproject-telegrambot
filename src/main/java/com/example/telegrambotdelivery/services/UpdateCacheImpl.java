package com.example.telegrambotdelivery.services;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.util.ArrayDeque;

@Service
public class UpdateCacheImpl implements UpdateCache {

    ArrayDeque<Update> updateCacheDeque = new ArrayDeque<>(20);

    InlineKeyboardMarkup previousInlineKeyboard = null;
    InlineKeyboardMarkup currentInlineKeyboard = null;
    Integer previousInlineMessageId = null;
    Integer currentInlineMessageId = null;

    @Override
    public Update getLastUsersUpdate(long chatid) {
        return null;
    }

    @Override
    public void addUpdateToCache(Update update) {
        if (updateCacheDeque.size() >= 15) {
            for (int i = 0; i < 10; i++) {
                updateCacheDeque.poll();
            }
        }
        updateCacheDeque.offer(update);
        if (update.hasCallbackQuery()) {
            InlineKeyboardMarkup newInlineKeyboard = update.getCallbackQuery().getMessage().getReplyMarkup();

            if (newInlineKeyboard.equals(currentInlineKeyboard)) {
                currentInlineKeyboard = newInlineKeyboard;
                currentInlineMessageId = update.getCallbackQuery().getMessage().getMessageId();
            }

            else {
                previousInlineKeyboard = currentInlineKeyboard;
                currentInlineKeyboard = newInlineKeyboard;
                previousInlineMessageId = currentInlineMessageId;
                currentInlineMessageId = update.getCallbackQuery().getMessage().getMessageId();
            }
        }
    }

    @Override
    public EditMessageReplyMarkup getPreviousInlineKeyboard(Integer lastInlineMessageId) {
        EditMessageReplyMarkup markup = new EditMessageReplyMarkup();
        markup.setMessageId(lastInlineMessageId);
        markup.setReplyMarkup(previousInlineKeyboard);

        return markup;
    }

    @Override
    public Integer getPreviousInlineMessageId() {
        return previousInlineMessageId;
    }

    @Override
    public String getLastCallBackCommand() {
        String command = "";
        for (Update update : updateCacheDeque) {
            if (update.hasCallbackQuery() &&
                update.getCallbackQuery().getMessage().getMessageId().equals(currentInlineMessageId)) {
                command = update.getCallbackQuery().getData();
            }
        }
        return command;
    }
}
