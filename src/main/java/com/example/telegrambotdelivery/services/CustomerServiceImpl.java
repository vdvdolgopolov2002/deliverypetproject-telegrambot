package com.example.telegrambotdelivery.services;

import com.example.telegrambotdelivery.entities.*;
import com.example.telegrambotdelivery.keyboards.InlineKeyboards;
import com.example.telegrambotdelivery.repositories.*;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;

@Service
public class CustomerServiceImpl implements CustomerService{

    CustomerRepository customerRepository;
    ProductRepository productRepository;
    CartRepository cartRepository;
    CartItemRepository cartItemRepository;
    OrderRepository orderRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, ProductRepository productRepository,
                               CartRepository cartRepository,  CartItemRepository cartItemRepository,
                               OrderRepository orderRepository) {
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public void registerNewCustomer(Update update, long chatid) throws TelegramApiException {
        if (!customerRepository.existsCustomerByChatid(chatid)){
            Customer customer = new Customer();
            customer.setChatid(chatid);
            customer.setName(update.getMessage().getFrom().getUserName());
            customer.setNumber("");
            customer.setPayment("");
            customer.setCustomerLocation("");
            customer.setCart(new Cart());
            customerRepository.save(customer);
        }
        else {
            throw new TelegramApiException("Ошибка регистрации, профиль уже существует");
        }
    }

    @Override
    public Customer getCustomerByChatID(long chatid) {
        if (customerRepository.existsCustomerByChatid(chatid)) {
            return customerRepository.getCustomerByChatid(chatid);
        }
        else{
            return null;
        }
    }

    @Override
    public String getCustomerProfileText(long chatid) throws TelegramApiException {
        if (customerRepository.existsCustomerByChatid(chatid)) {
            Customer customer = getCustomerByChatID(chatid);
            String profileText = ":bust_in_silhouette: " +  customer.getName() + "\n" + "\n" +
                    ":telephone_receiver: Номер: " + customer.getNumber() + "\n" + "\n" +
                    ":house_with_garden: Локация: " + "\n" + customer.getCustomerLocation() + "\n" +
                    ":credit_card: Номер карты: " + customer.getPayment();
            return profileText;
        }
        else {
            throw new TelegramApiException("Ошибка получения профиля");
        }
    }

    @Override
    public void updateCustomerPhone(long chatid, String number) throws TelegramApiException {

        if (isValidPhoneNumber(number) && customerRepository.existsCustomerByChatid(chatid)) {
            Customer customer = getCustomerByChatID(chatid);
            customer.setNumber(number);
            customerRepository.save(customer);
        }
        else throw new TelegramApiException("Ошибка обновления номера");
    }

    public boolean isValidPhoneNumber(String phoneNumber) {
        String pattern = "^(\\+7|8)\\d{10}$";
        return phoneNumber.matches(pattern);
    }

    @Override
    public void updateCustomerName(long chatid, String name) throws TelegramApiException {

        if (isValidName(name) && customerRepository.existsCustomerByChatid(chatid)) {
            Customer customer = getCustomerByChatID(chatid);
            customer.setName(name);
            customerRepository.save(customer);
        }
        else throw new TelegramApiException("Ошибка обновления имени");
    }

    public boolean isValidName(String name) {
        if (name == null) {
            return false;
        }
        if (name.length() > 50) {
            return false;
        }

        String[] words = name.split("\\s+");
        if (words.length > 4) {
            return false;
        }

        return true;
    }

    @Override
    public void updateCustomerLocation(long chatid, String location) throws TelegramApiException {

        if (isValidLocation(location) && customerRepository.existsCustomerByChatid(chatid)) {
            Customer customer = getCustomerByChatID(chatid);
            customer.setCustomerLocation(location);
            customerRepository.save(customer);
        }
        else throw new TelegramApiException("Ошибка обновления локации");
    }

    public boolean isValidLocation(String location) {
        if (location == null || location.isEmpty()) {
            return false;
        }

        if (location.length() > 100) {
            return false;
        }

        String[] words = location.split(", ");
        if (words.length < 3) {
            return false;
        }

        return true;
    }

    @Override
    public void updateCustomerPayment(long chatid, String payment) throws TelegramApiException {

        if (isValidPayment(payment) && customerRepository.existsCustomerByChatid(chatid)) {
            Customer customer = getCustomerByChatID(chatid);
            customer.setPayment(payment);
            customerRepository.save(customer);
        }
        else throw new TelegramApiException("Ошибка обновления способа оплаты");
    }

    @Override
    @Transactional
    public void addToCart(long chatid, Long productid) {
        Customer customer = customerRepository.getCustomerByChatid(chatid);
        Product product = productRepository.getProductById(productid);
        Cart cart = customer.getCart();

        CartItem cartItem = cart.getCartItems().stream()
                .filter(item -> item.getProduct().getId().equals(productid))
                .findFirst()
                .orElse(null);

        if (cartItem == null) {
            cartItem = new CartItem();
            cartItem.setProduct(product);
            cartItem.setQuantity(1);
            cartItemRepository.save(cartItem);
            cart.getCartItems().add(cartItem);
        } else {
            cartItem.setQuantity(cartItem.getQuantity() + 1);
            cartItemRepository.save(cartItem);
        }
        customerRepository.save(customer);
    }

    @Transactional
    public void removeFromCart(long chatid, Long productid) throws TelegramApiException {
        Customer customer = customerRepository.getCustomerByChatid(chatid);
        Product product = productRepository.getProductById(productid);
        Cart cart = customer.getCart();

        CartItem cartItem = cart.getCartItems().stream()
                .filter(item -> item.getProduct().equals(product))
                .findFirst()
                .orElseThrow(() -> new TelegramApiException("Ошибка удаления"));

        if (cartItem.getQuantity().equals(1)) {
            cart.getCartItems().remove(cartItem);
            cartItemRepository.delete(cartItem);
        }
        else {
            cartItem.setQuantity(cartItem.getQuantity() - 1);
            cartItemRepository.save(cartItem);
        }

        customerRepository.save(customer);
    }

    public boolean isValidPayment(String payment) {
        return true;
    }

    @Override
    public List<CartItem> getCustomersCartItems(long chatid) {
        Customer customer = getCustomerByChatID(chatid);
        Cart cart = customer.getCart();
        return cart.getCartItems();
    }

    @Transactional
    public List<BotApiMethod<?>> getCustomersCartMessages(long chatid) {
        List <BotApiMethod<?>> responseToSend = new ArrayList<>();
        Customer customer = getCustomerByChatID(chatid);
        List<CartItem> cartItems = getCustomersCartItems(chatid);

        StringBuilder sumText = new StringBuilder();
        sumText.append("Ваш заказ: " + "\n");

        Integer totalPrice = 0;

        for (CartItem cartItem : cartItems) {
            totalPrice = totalPrice + cartItem.getProduct().getPrice() * cartItem.getQuantity();

            StringBuilder itemText = new StringBuilder();
            itemText.append(cartItem.getProduct().getName() + "\n");
            itemText.append(cartItem.getProduct().getPrice() + "\n");
            itemText.append("Количество: " + cartItem.getQuantity().toString());

            sumText.append("\t" +cartItem.getQuantity() + " " + cartItem.getProduct().getName() + "\n");

            SendMessage itemMessage = new SendMessage();
            itemMessage.setChatId(chatid);
            itemMessage.setText(itemText.toString());
            itemMessage.setReplyMarkup(InlineKeyboards.cartItemKeyboard(cartItem));
            responseToSend.add(itemMessage);
        }
        sumText.append("\n" + "\n" + "Итоговая стоимость: " + totalPrice + "\n" + "Адрес доставки: " + customer.getCustomerLocation());

        SendMessage createOrderMessage = new SendMessage();
        createOrderMessage.setChatId(chatid);
        createOrderMessage.setText(sumText.toString());
        createOrderMessage.setReplyMarkup(InlineKeyboards.createOrderKeyboard());
        responseToSend.add(createOrderMessage);

        return responseToSend;
    }

    @Override
    @Transactional
    public void removeAllFromCart(long chatid) {
        Customer customer = customerRepository.getCustomerByChatid(chatid);
        Cart cart = customer.getCart();
        if (!cart.getCartItems().isEmpty()){
            for (CartItem cartItem : cart.getCartItems()) {
                cartItemRepository.delete(cartItem);
            }
            cart.setCartItems(Collections.emptyList());
        }

        customerRepository.save(customer);
    }

    @Override
    @Transactional
    public void createNewOrder(long chatid) throws TelegramApiException {
        Customer customer = customerRepository.getCustomerByChatid(chatid);
        Cart cart = customer.getCart();
        if (!cart.getCartItems().isEmpty() && isValidLocation(customer.getCustomerLocation()) && isValidPayment(customer.getPayment())){
            Integer totalPrice = 0;

            List<CartItem> orderItems = new ArrayList<>();

            for (CartItem cartItem : cart.getCartItems()) {
                totalPrice = totalPrice + cartItem.getProduct().getPrice() * cartItem.getQuantity();
                orderItems.add(cartItem);
            }

            Order newOrder = new Order();
            newOrder.setLocation(customer.getCustomerLocation());
            newOrder.setCustomersnumber(customer.getNumber());
            newOrder.setTotalPrice(totalPrice);
            newOrder.setStatus(OrderStatus.PROCESSING);
            newOrder.setOrderItems(orderItems);

            cartRepository.delete(cart);

            customer.setCart(new Cart());
            customer.getCart().setCartItems(Collections.emptyList());

            customerRepository.save(customer);
            orderRepository.save(newOrder);
        }
        else throw new TelegramApiException("Ошибка оформления заказа, заполните профиль и проверьте, что корзина не пуста");
    }

}
