package com.example.telegrambotdelivery.services;

import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

public interface UpdateCache {

    public Update getLastUsersUpdate(long chatid);

    public void addUpdateToCache(Update update);

    public EditMessageReplyMarkup getPreviousInlineKeyboard(Integer messageId);

    public Integer getPreviousInlineMessageId();

    String getLastCallBackCommand();
}
