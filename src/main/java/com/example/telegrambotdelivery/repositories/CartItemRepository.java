package com.example.telegrambotdelivery.repositories;

import com.example.telegrambotdelivery.entities.Cart;
import com.example.telegrambotdelivery.entities.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface CartItemRepository extends JpaRepository<CartItem, Long> {


}
