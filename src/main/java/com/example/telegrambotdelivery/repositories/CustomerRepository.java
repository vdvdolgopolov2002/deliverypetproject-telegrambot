package com.example.telegrambotdelivery.repositories;

import com.example.telegrambotdelivery.entities.Cart;
import com.example.telegrambotdelivery.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    public Customer getCustomerById(long id);

    public Customer getCustomerByChatid(long id);

    public Boolean existsCustomerByChatid(long chatid);

    public Cart getCartById(long id);

}
