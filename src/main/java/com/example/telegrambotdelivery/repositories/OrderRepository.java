package com.example.telegrambotdelivery.repositories;

import com.example.telegrambotdelivery.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
