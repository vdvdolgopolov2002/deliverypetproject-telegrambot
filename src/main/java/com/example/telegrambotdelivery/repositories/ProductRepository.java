package com.example.telegrambotdelivery.repositories;

import com.example.telegrambotdelivery.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> getAllBy();

    Product getProductById(Long id);

}
