package com.example.telegrambotdelivery.repositories;

import com.example.telegrambotdelivery.entities.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {


}
