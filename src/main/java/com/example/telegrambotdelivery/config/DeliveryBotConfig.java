package com.example.telegrambotdelivery.config;

import com.example.telegrambotdelivery.controllers.BackController;
import com.example.telegrambotdelivery.controllers.ControllerContainer;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;

import java.util.*;

import static com.example.telegrambotdelivery.controllers.Commands.*;

@Configuration
@PropertySource("classpath:application.properties")
@Data
@ComponentScan(basePackages = "com.example.telegrambotdelivery.controllers")
public class DeliveryBotConfig {

    @Value("${customerbot.name}")
    private String botName;

    @Value("${customerbot.token}")
    private String token;

    private List<BotCommand> botCommandList;

    @Autowired
    private ControllerContainer controllerContainer;

    @PostConstruct
    public void init(){
        botCommandList = new ArrayList<>();
        botCommandList.add(new BotCommand(START.getCommandName(), "зарегистрирует или очистит корзину"));
        botCommandList.add(new BotCommand(MENU.getCommandName(),"посмотреть меню"));
        botCommandList.add(new BotCommand("/cart","корзина"));
        botCommandList.add(new BotCommand("/payment","способ оплаты"));
        botCommandList.add(new BotCommand("/location","отправьте нам свою локацию"));
        botCommandList.add(new BotCommand(HELP.getCommandName(), "press it if u dumb"));
    }

}
