package com.example.telegrambotdelivery.keyboards;

import com.example.telegrambotdelivery.controllers.Commands;
import com.example.telegrambotdelivery.entities.CartItem;
import com.example.telegrambotdelivery.entities.Product;
import com.example.telegrambotdelivery.entities.ProductCategory;
import com.example.telegrambotdelivery.repositories.ProductRepository;
import com.example.telegrambotdelivery.services.ProductService;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.*;

@Component
public class InlineKeyboards {

    ProductService productService;

    @Autowired
    public InlineKeyboards(ProductService productService) {
        this.productService = productService;
    }

    public static InlineKeyboardMarkup getProfileInlineMarkup() {

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        //buttons
        InlineKeyboardButton phoneButton = new InlineKeyboardButton();
        phoneButton.setText(EmojiParser.parseToUnicode(":telephone_receiver: Телефон"));
        phoneButton.setCallbackData("/profile/phone");

        InlineKeyboardButton nameButton = new InlineKeyboardButton();
        nameButton.setText(EmojiParser.parseToUnicode(":bust_in_silhouette: Имя"));
        nameButton.setCallbackData("/profile/name");

        InlineKeyboardButton locationButton = new InlineKeyboardButton();
        locationButton.setText(EmojiParser.parseToUnicode(":house_with_garden: Локация"));
        locationButton.setCallbackData("/profile/location");

        InlineKeyboardButton paymentButton = new InlineKeyboardButton();
        paymentButton.setText(EmojiParser.parseToUnicode(":credit_card: Оплата"));
        paymentButton.setCallbackData("/profile/payment");

        //rows list
        //rows
        List<List<InlineKeyboardButton>> rowList= new ArrayList<>();

        List<InlineKeyboardButton> buttonsRow1 = new ArrayList<>();
        buttonsRow1.add(phoneButton);
        rowList.add(buttonsRow1);

        List<InlineKeyboardButton> buttonsRow2 = new ArrayList<>();
        buttonsRow2.add(nameButton);
        rowList.add(buttonsRow2);

        List<InlineKeyboardButton> buttonsRow3 = new ArrayList<>();
        buttonsRow3.add(locationButton);
        rowList.add(buttonsRow3);

        List<InlineKeyboardButton> buttonsRow4 = new ArrayList<>();
        buttonsRow4.add(paymentButton);
        rowList.add(buttonsRow4);


        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;

    }

    public static InlineKeyboardMarkup profileUpdateKeyborMaker(String command) {

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        //buttons
        InlineKeyboardButton updateButton = updateProfileButton(command);

        InlineKeyboardButton backButton = new InlineKeyboardButton();
        backButton.setText(EmojiParser.parseToUnicode(":leftwards_arrow_with_hook: Назад"));
        backButton.setCallbackData("/back");


        //rows list
        //rows
        List<List<InlineKeyboardButton>> rowList= new ArrayList<>();

        List<InlineKeyboardButton> buttonsRow1 = new ArrayList<>();
        buttonsRow1.add(updateButton);
        rowList.add(buttonsRow1);

        List<InlineKeyboardButton> buttonsRow2 = new ArrayList<>();
        buttonsRow1.add(backButton);
        rowList.add(buttonsRow2);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;

    }

    public static InlineKeyboardButton updateProfileButton (String command) {
        InlineKeyboardButton updateButton = new InlineKeyboardButton();

        switch(command){
            case "/profile/name":
                updateButton.setText(EmojiParser.parseToUnicode(":bust_in_silhouette: Изменить имя"));
                updateButton.setCallbackData(Commands.PROFILE_UPDATE.getCommandName() +"/name");
            break;

            case "/profile/phone":
                updateButton.setText(EmojiParser.parseToUnicode(":telephone_receiver: Изменить телефон"));
                updateButton.setCallbackData(Commands.PROFILE_UPDATE.getCommandName() +"/phone");
            break;

            case "/profile/location":
                updateButton.setText(EmojiParser.parseToUnicode(":house_with_garden: Изменить локацию"));
                updateButton.setCallbackData(Commands.PROFILE_UPDATE.getCommandName() +"/location");
            break;

            case "/profile/payment":
                updateButton.setText(EmojiParser.parseToUnicode(":credit_card: Изменить карту"));
                updateButton.setCallbackData(Commands.PROFILE_UPDATE.getCommandName() +"/payment");
            break;
        }

        return updateButton;
    }

    public static InlineKeyboardMarkup menuKeyboard() {

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowList= new ArrayList<>();

        InlineKeyboardButton menuButton1 = new InlineKeyboardButton();
        menuButton1.setText(EmojiParser.parseToUnicode(":hamburger: " + ProductCategory.BURGERS.getCategoryName()));
        menuButton1.setCallbackData("/menu/" + ProductCategory.BURGERS.getCategoryName());
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        row1.add(menuButton1);

        InlineKeyboardButton menuButton2 = new InlineKeyboardButton();
        menuButton2.setText(EmojiParser.parseToUnicode(":fries: " + ProductCategory.APPETISERS.getCategoryName()));
        menuButton2.setCallbackData("/menu/" + ProductCategory.APPETISERS.getCategoryName());
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        row2.add(menuButton2);

        InlineKeyboardButton menuButton3 = new InlineKeyboardButton();
        menuButton3.setText(EmojiParser.parseToUnicode(":hot_pepper: " + ProductCategory.SAUCE.getCategoryName()));
        menuButton3.setCallbackData("/menu/" + ProductCategory.SAUCE.getCategoryName());
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        row3.add(menuButton3);

        //rows
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;

    }

    public static InlineKeyboardMarkup menuCategoryKeyboard(List<Product> products) {

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList= new ArrayList<>();


        //buttons
        for (Product product : products) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            List<InlineKeyboardButton> row = new ArrayList<>();

            button.setText(EmojiParser.parseToUnicode("Добавить 1 " + product.getName()));
            button.setCallbackData("/order/" + product.getId().toString());

            row.add(button);
            rowList.add(row);
        }

        InlineKeyboardButton backButton = new InlineKeyboardButton();
        backButton.setText(EmojiParser.parseToUnicode(":leftwards_arrow_with_hook: Назад"));
        backButton.setCallbackData("/back");

        List<InlineKeyboardButton> backRow = new ArrayList<>();
        backRow.add(backButton);
        rowList.add(backRow);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;

    }

    public static InlineKeyboardMarkup cartItemKeyboard(CartItem cartItem) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList= new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();

        InlineKeyboardButton addButton = new InlineKeyboardButton();
        addButton.setText(EmojiParser.parseToUnicode(":heavy_plus_sign:"));
        addButton.setCallbackData("/cart/add/" + cartItem.getProduct().getId().toString());
        row.add(addButton);

        InlineKeyboardButton removeButton = new InlineKeyboardButton();
        removeButton.setText(EmojiParser.parseToUnicode(":heavy_minus_sign:"));
        removeButton.setCallbackData("/cart/remove/" + cartItem.getProduct().getId().toString());
        row.add(removeButton);

        rowList.add(row);
        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup createOrderKeyboard() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList= new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();

        InlineKeyboardButton createOrderButton = new InlineKeyboardButton();
        createOrderButton.setText(EmojiParser.parseToUnicode(":white_check_mark: Оформить заказ"));
        createOrderButton.setCallbackData("/cart/createorder");
        row1.add(createOrderButton);

        InlineKeyboardButton clearCartButton = new InlineKeyboardButton();
        clearCartButton.setText(EmojiParser.parseToUnicode(":basket: Очистить корзину"));
        clearCartButton.setCallbackData("/cart/clear");
        row2.add(clearCartButton);

        InlineKeyboardButton changeLocButton = new InlineKeyboardButton();
        changeLocButton.setText(EmojiParser.parseToUnicode(":world_map: Изменить адрес"));
        changeLocButton.setCallbackData(Commands.PROFILE_UPDATE.getCommandName() + "/location");
        row3.add(changeLocButton);
        rowList.addAll(Arrays.asList(row1, row2, row3));

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;
    }
}
